//
//  AppDelegate.h
//  Frans Recorder
//
//  Created by Vensi Developer on 11/26/13.
//  Copyright (c) 2013 Vensi Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "NavigationController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
