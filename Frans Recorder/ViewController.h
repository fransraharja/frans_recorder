//
//  ViewController.h
//  Frans Recorder
//
//  Created by Vensi Developer on 11/26/13.
//  Copyright (c) 2013 Vensi Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController
<AVAudioPlayerDelegate,AVAudioRecorderDelegate>
{
    AVAudioRecorder *audioRecorder;
    AVAudioPlayer *audioPlayer;
    UIButton *playButton;
    UIButton *recordButton;
    UIButton *stopButton;
}

@property (nonatomic, retain) IBOutlet UIButton *playButton;
@property (nonatomic, retain) IBOutlet UIButton *recordButton;
@property (nonatomic, retain) IBOutlet UIButton *stopButton;

-(IBAction) recordAudio;
-(IBAction) playAudio;
-(IBAction) stop;

@end
