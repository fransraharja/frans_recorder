//
//  main.m
//  Frans Recorder
//
//  Created by Vensi Developer on 11/26/13.
//  Copyright (c) 2013 Vensi Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
